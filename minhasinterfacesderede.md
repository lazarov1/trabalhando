1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether f4:b5:20:14:25:7e brd ff:ff:ff:ff:ff:ff
    inet 192.168.101.158/24 brd 192.168.101.255 scope global dynamic noprefixroute enp3s0
       valid_lft 7444sec preferred_lft 7444sec
    inet6 fe80::b8e7:a1d0:515c:65e7/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:1b:64:03:a3 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:1bff:fe64:3a3/64 scope link 
       valid_lft forever preferred_lft forever
4: br-20ab16f648f7: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:88:94:8e:16 brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.1/16 brd 172.18.255.255 scope global br-20ab16f648f7
       valid_lft forever preferred_lft forever
    inet6 fe80::42:88ff:fe94:8e16/64 scope link 
       valid_lft forever preferred_lft forever
16: vetha16b78e@if15: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether 32:ed:ac:72:44:8b brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::30ed:acff:fe72:448b/64 scope link 
       valid_lft forever preferred_lft forever
18: veth97a54ca@if17: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether a2:35:78:80:b0:f4 brd ff:ff:ff:ff:ff:ff link-netnsid 1
    inet6 fe80::a035:78ff:fe80:b0f4/64 scope link 
       valid_lft forever preferred_lft forever
55: br-7596436919b0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:d3:ae:49:ea brd ff:ff:ff:ff:ff:ff
    inet 172.16.239.1/24 brd 172.16.239.255 scope global br-7596436919b0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:d3ff:feae:49ea/64 scope link 
       valid_lft forever preferred_lft forever
56: br-ca10ab6ca3f6: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:11:6e:24:c9 brd ff:ff:ff:ff:ff:ff
    inet 172.16.238.1/24 brd 172.16.238.255 scope global br-ca10ab6ca3f6
       valid_lft forever preferred_lft forever
    inet6 fe80::42:11ff:fe6e:24c9/64 scope link 
       valid_lft forever preferred_lft forever
57: br-3bb1416f9709: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:7e:d2:53:74 brd ff:ff:ff:ff:ff:ff
    inet 172.19.0.1/16 brd 172.19.255.255 scope global br-3bb1416f9709
       valid_lft forever preferred_lft forever
    inet6 fe80::42:7eff:fed2:5374/64 scope link 
       valid_lft forever preferred_lft forever
117: veth3efdacc@if116: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether f6:69:ae:79:8b:a3 brd ff:ff:ff:ff:ff:ff link-netnsid 4
    inet6 fe80::f469:aeff:fe79:8ba3/64 scope link 
       valid_lft forever preferred_lft forever
